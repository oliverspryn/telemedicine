# Telemedicine

[![Netlify Status](https://api.netlify.com/api/v1/badges/be7f0056-9ac6-4a1f-b33f-963f52445264/deploy-status)](https://app.netlify.com/sites/oliverspryn-telemedicine/deploys)

A simple website to host the MyUPMC telemedicine promotional video and shim it with the necessary metadata to embed it on other sites such as LinkedIn.
